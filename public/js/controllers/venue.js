'use strict';

angular.module('wifiapp.venues').controller('VenueDetailCtrl', ['$rootScope', '$scope', '$http', '$stateParams', '$location', '$debounce', '$ionicPopup', '$ionicLoading', 'config', 'Global',
    function($rootScope, $scope, $http, $stateParams, $location, $debounce, $ionicPopup, $ionicLoading, config, Global) {
        $scope.items = [];
        $scope.page = 1; // result is the same with page = 1;
        $scope.busy = false;
        $scope.dataOver = false;

        // Captions templates
        var captions = [
            'Hey, latest wifi here is “[wifi password]”',
            'Updated wifi here “[wifi password]” guys!',
            'Come on, wifi is “[wifi password]”',
            'Wow, wifi here “[wifi password]”'
        ];

        /**
         * Get Venue Info
         * @trihuynh
         */

        function getVenueInfo() {
            $scope.loading = $ionicLoading.show({
                content: 'Loading',
            });
            // console.log('Page: ' + $scope.page);
            $scope.busy = true;
            $http.get(config.HOST_IP + '/venue/' + $stateParams.venueId + '/wifipw', {
                params: {
                    page: $scope.page
                }
            }).success(function(data) {
                if (data) {
                    if (data.venue)
                        $scope.data = data.venue;
                    if (data.items)
                        for (var i = 0; i < data.items.length; i++) {
                            $scope.items.push(data.items[i]);
                        }
                    $scope.dataOver = (data.items.length < data.limit) || data.items.length === 0 ? true : false;
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                    $scope.page++;
                }
                $scope.loading.hide();
            }).error(function() {
                $scope.busy = false;
                alert('Connection is incorrupt! Please try again later!');
                // $location.path('/');
                $scope.loading.hide();
            });
        };
        $scope.$on('ngRepeatFinished', function(ngRepeatFinishedEvent) {
            $scope.busy = false;
        });
        /**
         * Loadmore venues
         * @TriHuynh
         */
        $scope.loadMore = function() {
            if ($scope.busy) return false;
            getVenueInfo();
        };

        /**
         * Like Wifi Password
         * @trihuynh
         */
        $scope.like = function(index) {
            $rootScope.requireLogin(function() {
                if ($scope.items[index]) {
                    $scope.items[index].busy = true;
                    $http.put(config.HOST_IP + '/api/wifipw/' + $scope.items[index]._id + '/like').success(function(data) {
                        if (data.liked) $scope.items[index].likes++;
                        else $scope.items[index].likes--;
                        $scope.items[index].liked = data.liked;
                        $scope.items[index].busy = false;
                    }).error(function() {
                        $scope.items[index].busy = false;
                        // alert('Connection is incorrupt! Please try again later!');
                        // $location.path('/');
                    });
                }
            });
        };

        /**
         * Share Wifi Password
         * @trihuynh
         */

        $scope.sharePw = function() {
            $rootScope.requireLogin(function() {
                $scope.input = {}
                var caption = captions[Math.floor((Math.random() * 10) % captions.length)];
                $ionicPopup.show({
                    templateUrl: 'popup-template.html',
                    title: caption,
                    subTitle: 'Share now :',
                    inputType: 'password',
                    scope: $scope,
                    buttons: [{
                        text: '',
                        type: 'left',
                        onTap: function(e) {
                            return {
                                skip: true
                            };
                        }
                    }, {
                        text: 'done',
                        type: 'right button-positive',
                        onTap: function(e) {
                            return $scope.input;
                        }
                    }, ]
                }).then(function(res) {
                    if (!res.skip)
                        if (!res.password || res.password.length < 8) {
                            alert('Password must be more than 8 characters');
                        } else
                    if (res.password.indexOf(' ') >= 0) {
                        alert('Password cannot contain space characters')
                    } else {
                        $scope.loading = $ionicLoading.show({
                            content: 'Loading',
                        });
                        res.password = caption.replace('[wifi password]', res.password);
                        $http.post(config.HOST_IP + '/api/venue/' + $stateParams.venueId + '/wifipw', res).success(function(response) {
                            if (response && !response.error) {
                                $scope.items.push(response);
                                $scope.loading.hide();
                                // SHARE FACEBOOK CODE HERE
                                // var link = 'http://m.wifi.com/#!/venue/' + $stateParams.venueId;
                                $ionicPopup.confirm({
                                    title: 'Almost done! Congratulations and thank you! Would you like to share this on your facebook ?',
                                    cancelText: 'NO',
                                    cancelType: 'btn_sharefb button-light',
                                    okText: 'YES',
                                    okType: 'btn_sharefb yes button-light',
                                }).then(function(res) {
                                    if (res) {
                                        FB.api('/me/feed', 'post', {
                                            caption: 'Wifi Password Sharing Application',
                                            description: response.password,
                                            link: 'http://wifiapp.heroku.com',
                                            name: $scope.data.name,
                                            message: 'Free wifi for everybody :3'
                                        }, function(result) {
                                            if (!result || result.error) {
                                                // alert('Error occured');
                                                // console.log(result)
                                            } else {
                                                // alert('Post ID: ' + result.id);
                                            }
                                        });
                                    } else {
                                        // alert('Cancel');
                                    }
                                });
                                // -- END SHARE FACEBOOK

                            };
                        }).error(function(data) {
                            $scope.loading.hide();
                            alert(data.error);
                            console.warn('Connection is incorrupt!')
                        });
                    }
                });
            });
        };
        // END
    }
]);