'use strict';

function url_base64_decode(str) {
    var output = str.replace('-', '+').replace('_', '/');
    switch (output.length % 4) {
        case 0:
            break;
        case 2:
            output += '==';
            break;
        case 3:
            output += '=';
            break;
        default:
            throw 'Illegal base64url string!';
    }
    return window.atob(output); //polifyll https://github.com/davidchambers/Base64.js
}

angular.module('wifiapp.system')
/**
 * Controller Login
 * @Tham khao de gui fb token len server + nhan token auth cua ser
 */
.controller('HomeCtrl', ['$rootScope', '$scope', '$http', '$stateParams', '$location', '$debounce', '$ionicPopup', '$ionicLoading', 'config', 'Global',
    function($rootScope, $scope, $http, $stateParams, $location, $debounce, $ionicPopup, $ionicLoading, config, Global) {
        $scope.global = Global;
        $scope.input = {};
        $scope.items = [];
        $scope.page = 1; // result is the same with page = 1;
        $scope.busy = false;
        $scope.dataOver = false;

        $rootScope.getGeolocation(3000, 1000);

        /**
         * Explore venues
         * @TriHuynh
         */
        
        function explore() {
            $scope.dataOver = false;
            $scope.busy = true;
            var params = {
                lat: $rootScope.currPos.lat,
                lng: $rootScope.currPos.lng,
                page: $scope.page
            };
            if ($scope.input.search) params.query = $scope.input.search;
            $http.get(config.HOST_IP + '/venues/explore', {
                params: params
            }).
            success(function(data) {
                for (var i = 0; i < data.items.length; i++) {
                    $scope.items.push(data.items[i]);
                }
                $scope.dataOver = (data.items.length < data.limit) || data.items.length === 0 ? true : false;
                if ($scope.refresh) {
                    $scope.$broadcast('scroll.refreshComplete');
                    $scope.refresh = false;
                }
                $scope.$broadcast('scroll.infiniteScrollComplete');
                $scope.page++;
                $scope.busy = false;
            }).error(function(data, status, headers, config) {
                $scope.busy = false;
                console.error('Connection is intercrupt!');
            });
        }

        /**
         * Re-explore with keywork after 1s since the queryInput entered
         * @TriHuynh
         */

        $scope.explore = function(refresh) {
            $scope.refresh = refresh;
            $scope.items = [];
            $scope.page = 1;
            if (!refresh)
                return explore();
            else            
                $debounce(explore, 500);
        };

        $scope.$on('ngRepeatFinished', function(ngRepeatFinishedEvent) {
            $scope.busy = false;
        });
        /**
         * Loadmore venues
         * @TriHuynh
         */
        $scope.loadMore = function() {
            if ($scope.busy) return false;            
            explore();
        };
        /**
         * Add new venue
         * @trihuynh
         */
        $scope.addVenue = function() {
            $rootScope.requireLogin(function() {
                $scope.data = {
                    name: $scope.input.search,
                    lat: $rootScope.currPos.lat,
                    lng: $rootScope.currPos.lng
                };

                $ionicPopup.show({
                    templateUrl: 'new_place.html',
                    scope: $scope,
                    buttons: [{
                        text: '',
                        type: 'left',
                        onTap: function(e) {
                            return {
                                skip: true
                            };
                        }
                    }, {
                        text: 'done',
                        type: 'right button-positive',
                        onTap: function(e) {
                            return $scope.data;
                        }
                    }, ]
                }).then(
                    function(res) {
                        if (!res.skip) {
                            $scope.loading = $ionicLoading.show({
                                content: 'Loading',
                            });
                            $http.post(config.HOST_IP + '/api/venue', res).success(function(response) {
                                if (response.item && !response.error) {
                                    $scope.items.push(response.item);
                                }
                                $scope.loading.hide();
                            }).error(function() {
                                console.warn('Connection is incorrupt!');
                                $scope.loading.hide();
                            });
                        }
                    }
                );
            });
        };

        // END
    }
]);