'use strict';

angular.module('wifiapp', ['ionic', 'pasvaz.bindonce', 'wifiapp.system', 'wifiapp.venues'])
    .constant('config', {
        'DEBUG': false,
        'FB_API': '278519858991132',
        // 'HOST_IP': 'http://localhost:3000',
        'HOST_IP': ''
    })
    .run(['$rootScope', '$window', '$location', '$http', '$ionicLoading', '$ionicPopup', 'config', 'Global',
        function($rootScope, $window, $location, $http, $ionicLoading, $ionicPopup, config, Global) {

            /**
             * Auth with Backend
             * @trihuynh
             */

            function auth(accessToken, cb) {
                $http.post(config.HOST_IP + '/auth/facebook', {
                    accessToken: accessToken
                }).success(function(data) {
                    // Login
                    if (data && data.token) {
                        $window.localStorage.token = data.token;
                        Global.authenticated = true;
                        var encodedProfile = data.token.split('.')[1];
                        Global.user = JSON.parse(url_base64_decode(encodedProfile));
                        if (cb) return cb();
                    }
                }).error(function() {
                    delete $window.localStorage.token;
                    Global.authenticated = false;
                    console.error('Incorrupt connection!');
                });
            }


            /**
             * Facebook Login Dialog
             * @trihuynh
             */

            function loginFacebook(cb) {
                FB.login(function(response) {
                    if (response.authResponse) {
                        auth(response.authResponse.accessToken, cb);
                    } else {
                        console.log('User cancelled login or did not fully authorize.');
                    }
                }, {
                    scope: 'email,publish_actions'
                });
            }

            /**
             * Get Facebook Login Status
             * @trihuynh
             */
            function authFacebook(cb) {
                FB.getLoginStatus(function(response) {
                    if (response.status === 'connected') {
                        return auth(response.authResponse.accessToken, cb)
                    } else if (response.status === 'not_authorized') {
                        // the user is logged in to Facebook but has not authenticated your app
                        return loginFacebook(cb);
                    } else { // the user isn't logged in to Facebook.
                        return loginFacebook(cb);
                    }
                });
            };


            /**
             * Login
             * @trihuynh
             */
            $rootScope.login = function(cb) {
                // SKIP LOGIN
                if (config.DEBUG) {
                    $http.get(config.HOST_IP + '/auth/fakeToken').success(function(data) {
                        // Login
                        if (data && data.token) {
                            $window.localStorage.token = data.token;
                            Global.authenticated = true;
                            var encodedProfile = data.token.split('.')[1];
                            Global.user = JSON.parse(url_base64_decode(encodedProfile));
                        }
                    }).error(function() {
                        delete $window.localStorage.token;
                        Global.authenticated = false;
                        console.error('Incorrupt connection!');
                    });
                } else {
                    // LOGIN (viet ham login FB o day)                                        
                    $ionicPopup.show({
                        templateUrl: 'loginfb.html',
                        title: 'To comment or do anything on this community. You should login first :',
                        buttons: [{
                            text: '',
                            type: 'left',
                            onTap: function(e) {
                                return true;
                            }
                        }, {
                            text: '<img src="./img/fb41.png">',
                            type: 'right button-positive',
                            onTap: function(e) {
                                return 'login';
                            }
                        }]
                    }).then(function(res) {
                        if (res === 'login'){
                            authFacebook(cb);
                        }
                    }, function(err) {
                        console.log('Err:', err);
                    }, function(msg) {
                        // console.log('message:', msg);
                    });
                }
            };

            $rootScope.requireLogin = function(cb) {
                if (!$window.localStorage.token)
                    $rootScope.logout(false, cb);
                else return cb();
            }

            /**
             * Logout
             * @trihuynh
             */
            $rootScope.logout = function(skiplogin, cb) {
                Global.authenticated = false;
                Global.user = null;
                delete $window.localStorage.token;
                /* RE-LOGIN */
                if (!skiplogin)
                    $rootScope.login(cb);
            };

            if (!$window.localStorage.token)
                $rootScope.logout(true);

            // Set default position
            $rootScope.currPos = {
                // TimeSquare
                lat: 40.756497,
                lng: -73.986904
            }

            function setPosition(position) {
                $rootScope.currPos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
            };

            $rootScope.getGeolocation = function(maximumAge, timeout) {
                // Cache: 3s
                // Timeout: 5s
                navigator.geolocation.getCurrentPosition(setPosition, function(e) {}, {
                    maximumAge: maximumAge,
                    timeout: timeout,
                    enableHighAccuracy: true
                });
            }
            // Get geolocation
            $rootScope.getGeolocation(3000, 5000);

            document.addEventListener("deviceready", function() {
                $rootScope.$apply(function() {
                    // Watch geolocation every 5s
                    navigator.geolocation.watchPosition(setPosition, function(e) {}, {
                        timeout: 5000
                    });
                });
            }, false);

        }
    ]);

angular.module('wifiapp.system', []);
angular.module('wifiapp.venues', []);