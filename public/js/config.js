'use strict';

//Setting up route
angular.module('wifiapp').config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {
        // For unmatched routes:
        $urlRouterProvider.otherwise('/');

        // states for my app
        $stateProvider
            .state('venue by id', {
                url: '/venue/:venueId',
                templateUrl: 'views/venues/view.html',
                controller: 'VenueDetailCtrl'
            })
            .state('home', {
                url: '/',
                templateUrl: 'views/index.html',
                controller: 'HomeCtrl'
            })
            // .state('login', {
            //     url: '/login',
            //     templateUrl: 'views/login.html',
            //     controller: 'LoginCtrl '
            // });
    }
]);

//Setting HTML5 Location Mode
angular.module('wifiapp').config(['$locationProvider',
    function($locationProvider) {
        // $locationProvider.hashPrefix('!');
    }
]);

// Attach token into request
angular.module('wifiapp').config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
});
