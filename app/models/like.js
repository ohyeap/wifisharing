'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * User Schema
 */
var LikeSchema = new Schema({
    // type: {
    //     type: String,
    //     enum: {
    //         values: 'wifipw venue'.split(' '),
    //         message: '{PATH} value is not in `{VALUE}`'
    //     },
    //     default:'wifipw',
    //     unique: true
    // },
    _user:{
        type: Schema.ObjectId,
        ref: 'User'
    },
    _wifipw: {
        type: Schema.ObjectId,
        ref: 'WifiPw'
    },
    _venue: {
        type: Schema.ObjectId,
        ref: 'Venue'
    },
    createTime: {
        type: Date,
        default: Date.now()
    }
},{ collection: 'likes' });

mongoose.model('Like', LikeSchema);