'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * User Schema
 */
var UserSchema = new Schema({
    fbId: {
        type: Number,
        required: true,
        unique: true
    },
    profile: Schema.Types.Mixed,
    createTime: {
        type: Date,
        default: Date.now()
    },
    updateTime: {
        type: Date,
        default: Date.now()
    }
},{ collection: 'users' });

mongoose.model('User', UserSchema);