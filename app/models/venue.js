'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


/**
 * Venue Schema
 */
var VenueSchema = new Schema({
    name: {
        type: String,
        default: '',
        trim: true
    },
    location: Schema.Types.Mixed,
    // categories: [Schema.Types.Mixed],
    createTime: {
        type: Date,
        default: Date.now
    },
    updateTime: {
        type: Date,
        default: Date.now
    }
}, {
    collection: 'venues'
});

/**
 * Virtuals
 */
VenueSchema.virtual('id').set(function(id) {
    this._id = id;
}).get(function() {
    return this._id;
});

/**
 * Statics
 */
VenueSchema.statics = {
    load: function(id, cb) {
        this.findOne({
            _id: id
        }).exec(cb);
    }
};

mongoose.model('Venue', VenueSchema);
