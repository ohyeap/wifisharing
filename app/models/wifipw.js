'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Wifi Password Schema
 */
var WifiPwSchema = new Schema({    
    password: {
        type: String,
        default: '',
        required: '{PATH} is required!'
    },
    _venue: {
        type: Schema.ObjectId,
        ref: 'Venue'
    },
    _user:{
        type: Schema.ObjectId,
        ref: 'User'
    },
    createTime: {
        type: Date,
        default: Date.now
    },
    updateTiem: {
        type: Date,
        default: Date.now
    }
});
/**
 * Validations
 */
var validateString = function(value, min, max) {
    if(!value || !value.length){               
        return false;
    }
    if(min)
        if(!value.length >= min) return false;
    if(max)
        if(!value.length <= max) return false;
    return true;    
};
WifiPwSchema.path('password').validate(function(password) {        
    // if you are authenticating by the  local oauth strategies, don't validate
    return (typeof password === 'string' && validateString(password,8));
}, 'Password must be more than 8 characters');
/**
 * Statics
 */
WifiPwSchema.statics = {
    load : function(id, cb) {
        this.findOne({
            _id: id
        }).populate('_venue', 'name location').exec(cb);
    }
};

mongoose.model('WifiPw', WifiPwSchema);