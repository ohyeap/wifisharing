'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


/**
 * User Schema
 */
var SettingSchema = new Schema({
    foursquare_token: String,
},{ collection: 'settings' });

mongoose.model('Setting', SettingSchema);