'use strict';



/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    config = require('../../config/config'),
    async = require('async'),
    graph = require('fbgraph'),
    foursquare = require('node-foursquare')({
        secrets: config.foursquare
    }),
    _ = require('lodash'),
    jwt = require('jsonwebtoken'),
    User = mongoose.model('User'),
    SettingModel = mongoose.model('Setting');

/**
 * Send User Profile
 * @trihuynh
 */
exports.me = function(req, res) {
    res.jsonp(req.user || null);
};

/**
 * Fake Token
 * @trihuynh
 */
exports.fakeToken = function(req, res) {
    User.findOne().lean().exec(function(e1, user) {
        var token = jwt.sign(_.assign({
            _id: user._id,
            fbId: user.fbId
        }, _.pick(user.profile, 'name')), config.sessionSecret, {
            expiresInMinutes: 60 * 5
        });
        return res.jsonp({
            token: token
        });
    });
};

/**
 * Find user by id
 * @trihuynh
 */
exports.load = function(req, res, next, id) {
    User.findOne({
        _id: id
    }).exec(function(err, profile) {
        if (err || !user) return res.status(404).jsonp({
            error: err.errors[0]
        });
        req.profile = profile;
        next();
    });
};

/**
 * Renew Token
 * @trihuynh
 */
exports.renewtoken = function(req, res) {
    // verify the existing token
    var profile = jwt.verify(req.body.token, secret);

    // if more than 14 days old, force login
    if (profile.iat - new Date() > 14) { // iat == issued at
        return res.send(401); // re-logging
    }

    // issue a new token
    var refreshed_token = jwt.sign(profile, secret, {
        expiresInMinutes: 60 * 5
    });

    res.jsonp({
        token: refreshed_token
    });
};

/**
 * Auth via Facebook
 * @trihuynh
 */
exports.authFacebook = function(req, res) {
    var token = req.body.accessToken || null;

    if (!token)
    // Khong du dieu kien Auth
        return res.status(400).send();
    // Lay Long Time Token
    graph.extendAccessToken({
        "access_token": token,
        "client_id": config.facebook.clientID,
        "client_secret": config.facebook.clientSecret
    }, function(err, response) {
        if (err) return res.status(401).send();
        graph.setAccessToken(response.access_token);
        graph.get("/me?fields=name,email,gender,username,location,hometown,link,picture.type(small),birthday,locale", function(e1, d1) {
            if (e1) {
                // Loi khong the truy van thong tin FB User
                console.error(e1);
                return res.status(401).send();
            }
            if (d1 && d1.id) {
                User.findOne({
                    fbId: d1.id
                }).exec(function(e2, d2) {
                    if (e2) console.error(e2);
                    if (d2) {
                        // UPDATE
                        d2.profile = _.assign(d1, response);
                        d2.updateTime = new Date();
                        d2.save(function(e3) {
                            var token = jwt.sign(_.assign({
                                _id: d2._id,
                                fbId: d2.fbId
                            }, _.pick(d2.profile, 'name')), config.sessionSecret, {
                                expiresInMinutes: 60 * 5
                            });
                            return res.jsonp({
                                token: token
                            });
                        })
                    } else {
                        // CREATE NEW
                        var user = new User({
                            fbId: d1.id,
                            profile: _.assign(d1, response)
                        });
                        user.save(function(e3) {

                            var token = jwt.sign(_.assign({
                                _id: user._id,
                                fbId: user.fbId
                            }, _.pick(user.profile, 'name')), config.sessionSecret, {
                                expiresInMinutes: 60 * 5
                            });
                            return res.jsonp({
                                token: token
                            });
                        })
                    }
                })
            }
        });
    });
};

/**
 * Get Foursquare auth -> redirect to callback link
 * @trihuynh
 */
exports.authFoursquare = function(req, res) {
    res.writeHead(303, {
        'location': foursquare.getAuthClientRedirectUrl()
    });
    res.end();
};

/**
 * Get accesstoken from callback code
 * @trihuynh
 */
exports.callbackFoursquare = function(req, res) {
    foursquare.getAccessToken({
        code: req.query.code
    }, function(error, accessToken) {
        if (error) {
            res.send(false);
        } else {
            // Save the accessToken
            SettingModel.findOneAndUpdate({
                foursquare_token: {
                    $exists: true
                }
            }, {
                foursquare_token: accessToken
            }, {
                upsert: true
            }, function(e1, d1) {
                if (e1) console.error(e1);
                res.send(true);
            });
        }
    });
};