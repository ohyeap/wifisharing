'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    WifiPwModel = mongoose.model('WifiPw'),
    LikeModel = mongoose.model('Like'),
    async = require('async'),
    moment = require('moment'),
    _ = require('lodash');

/**
 * Show an WifiPw
 * @trihuynh
 */
exports.getOne = function(req, res) {
    res.jsonp(req.WifiPw);
};
/**
 * Load WifiPw by id
 * @trihuynh
 */
exports.load = function(req, res, next, id) {
    WifiPwModel.load(id, function(err, doc) {
        if (err || !doc) return res.status(404).jsonp({
            error: 'Not found'
        });
        req.WifiPw = doc;
        next();
    });
};

/**
 * List of WifiPw
 * @trihuynh
 */
exports.getAll = function(req, res) {
    var params = {};
    params.limit = req.param('limit') ? parseInt(req.param('limit')) : 10;
    params.offset = req.param('page') ? (parseInt(req.param('page')) * params.limit - params.limit) : 0;

    async.parallel({
            total: function(cb1) {
                WifiPwModel.count({
                    _venue: req.venue._id
                }).exec(function(e, n) {
                    if (e) console.error(e);
                    cb1(null, n);
                });
            },
            items: function(cb1) {
                WifiPwModel.find({
                    _venue: req.venue._id
                }).sort('createTime').populate('_user', '_id profile.name profile.picture').skip(params.offset).limit(params.limit).lean().exec(function(err, items) {
                    if (err) {
                        return res.status(500).jsonp({
                            error: 'Unknow'
                        })
                    } else {
                        async.map(items, function(item, callback) {
                            async.parallel({
                                    createTime: function(cb) {
                                        if (item.createTime) {
                                            if (moment().diff(moment(item.createTime), 'days', true) < 30)
                                                cb(null, moment(item.createTime).fromNow());
                                            else
                                                cb(null, moment(item.createTime).format("DD-MM-YYYY"));
                                        } else cb(null, null);
                                    },
                                    likes: function(cb) {
                                        LikeModel.count({
                                            _wifipw: item._id
                                        }, function(e1, count) {
                                            if (e1) console.error(e1);
                                            cb(null, count);
                                        });
                                    },
                                    liked: function(cb) {
                                        if (req.user)
                                            LikeModel.count({
                                                _wifipw: item._id,
                                                _user: req.user._id
                                            }, function(e1, count) {
                                                if (e1) console.error(e1);
                                                cb(null, !! count);
                                            });
                                        else cb(null, false);
                                    },
                                },
                                function(e2, d2) {
                                    if (e2) console.log(e2);
                                    callback(null, _.assign(item, d2));
                                });
                        }, function(e3, d3) {
                            cb1(null, d3);
                        });
                    }
                });
            }
        },
        function(err, results) {
            return res.jsonp(_.assign({
                venue: req.venue
            }, results, params));
        });
};

/**
 * Create an WifiPw
 * @trihuynh
 */
exports.create = function(req, res) {
    var WifiPw = new WifiPwModel(req.body);
    WifiPw._user = req.user._id;
    WifiPw._venue = req.venue._id;
    WifiPw.save(function(err) {
        if (err) {
            return res.status(500).jsonp({
                error: _.values(err.errors)[0].message
            });
        } else {
            var time = WifiPw.createTime;
            if (moment().diff(moment(time), 'days', true) < 30)
                time = moment(time).fromNow();
            else
                time = moment(time).format("DD-MM-YYYY");
            res.jsonp(_.assign(WifiPw.toObject(), {
                createTime: time,
                liked: false,
                likes: 0
            }));
        }
    });
};

/**
 * Update an WifiPw
 * @trihuynh
 */
exports.update = function(req, res) {
    var WifiPw = req.WifiPw;
    WifiPw = _.extend(WifiPw, req.body);

    WifiPw.save(function(err) {
        if (err) {
            return res.status(500).jsonp({
                error: err.errors[0]
            });
        } else {
            res.jsonp(WifiPw);
        }
    });
};

/**
 * Delete an WifiPw
 * @trihuynh
 */
exports.destroy = function(req, res) {
    var WifiPw = req.WifiPw;

    WifiPw.remove(function(err) {
        if (err) {
            return res.status(500).jsonp({
                error: err.errors[0]
            });
        } else {
            res.jsonp(WifiPw);
        }
    });
};
/**
 * Like/dislike an WifiPw
 * @trihuynh
 */
exports.like = function(req, res) {
    LikeModel.findOne({
        _wifipw: req.WifiPw._id,
        _user: req.user._id
    }).exec(function(e1, d1) {
        if (e1) console.error(e1);
        if (d1) {
            // Dislike
            d1.remove(function(e2) {
                return res.jsonp({
                    liked: false
                });
            })
        } else {
            // Like
            var Like = new LikeModel({
                _wifipw: req.WifiPw._id,
                _user: req.user._id
            });
            Like.save(function(e2) {
                return res.jsonp({
                    liked: true
                });
            })
        }
    });
};