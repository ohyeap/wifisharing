'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    config = require('../../config/config'),
    async = require('async'),
    foursquare = require('node-foursquare')({
        secrets: config.foursquare
    }),
    _ = require('lodash'),
    SettingModel = mongoose.model('Setting'),
    VenueModel = mongoose.model('Venue'),
    WifiPwModel = mongoose.model('WifiPw');


/**
 * Explore venues by lat/long or location name
 * @trihuynh
 * test: http://localhost:3000/venues/explore?ll=16.059276,108.213347
 */
exports.explore = function(req, res) {
    
    if (req.param('lat') && req.param('lng')) {
        var params = {
            radius: 1000, // 100 meters
            limit: 10, // max 50
            offset: 0,
            sortByDistance: 1 // sort by distance
        };
        if (parseInt(req.param('page'))) // If page > 0
            params.offset = req.param('page') * params.limit - params.limit;
        if (req.param('query')) {
            params.query = req.param('query');
        }
        // ------------------- GET ACCESS TOKEN

        SettingModel.findOne().select('foursquare_token').lean().exec(function(e1, d1) {
            if (e1) console.error(e1);
            if (d1 && d1.foursquare_token) {
                // console.log(params);
                //https://developer.foursquare.com/docs/venues/explore               
                foursquare.Venues.explore(req.param('lat'), req.param('lng'), params, d1.foursquare_token, function(error, result) {
                    if (error) console.error(error);
                    if (result && result.groups && result.groups[0].items) {
                        var resource = _.pluck(result.groups[0].items, 'venue');
                        async.map(resource, function(obj, callback) {
                            WifiPwModel.findOne({
                                _venue: obj.id
                            }).sort('-createTime').populate('_user', 'profile.name').lean().exec(function(e2, d2) {
                                callback(null, _.assign(_.pick(obj, 'id', 'name', 'location'), {
                                    wifipw: d2
                                }));
                            });
                        }, function(err, items) {
                            // Respone items list
                            res.jsonp(_.assign({
                                items: items
                            }, params));
                            // Find and save venue to data
                            async.each(items, function(item, callback) {
                                VenueModel.findByIdAndUpdate(item.id, item, {
                                    upsert: true
                                }, function() {
                                    callback(null, true);
                                });
                            });
                        });
                    } else return res.jsonp(_.assign({
                        items: null
                    }, params));
                });
            } else res.jsonp({
                error: 'No foursquare access_token'
            });
        });
    } else {
        return res.status(400).jsonp({
            error: 'Latitude and longitude of the user\'s location id required'
        });
    }
};
/**
 * Create an Venue
 * @trihuynh
 */
exports.create = function(req, res) {
    if (!(req.body.name && req.body.lat && req.body.lng))
        return res.status(400).jsonp({
            error: 'Venue name and lat/lng are required'
        });
    // Captions templates
    var captions = [
        'Hey, latest wifi here is “[wifi password]”',
        'Updated wifi here “[wifi password]” guys!',
        'Come on, wifi is “[wifi password]”',
        'Wow, wifi here “[wifi password]”'
    ];
    var caption = captions[Math.floor((Math.random() * 10) % captions.length)].replace('[wifi password]', req.body.password);
    // Params
    var params = req.body;
    params.ll = req.body.lat + ',' + req.body.lng;
    delete params.lat;
    delete params.lng;
    delete params.password;

    /*
        1. Create foursquare venue
        2. Add it to data
        3. Create Wifipass
     */

    SettingModel.findOne().select('foursquare_token').lean().exec(function(e1, d1) {
        if (e1) console.error(e1);
        if (d1 && d1.foursquare_token) {
            // https://developer.foursquare.com/docs/venues/add            
            foursquare.Venues.add(params, d1.foursquare_token, function(err, result) {
                if (err) console.error(err);
                if (result && result.venue) {
                    var data = _.pick(result.venue, 'id', 'name', 'location');
                    VenueModel.findByIdAndUpdate(data.id, data, {
                        upsert: true
                    }, function(e2, d2) {
                        if (e2) console.log(e2);
                        var WifiPw = new WifiPwModel({
                            password: caption,
                            _venue: data.id,
                            _user: req.user._id
                        });
                        WifiPw.save(function(e3, d3) {
                            if (e3) console.error(e3);
                            WifiPwModel.findById(d3._id).populate('_user', 'profile.name').lean().exec(function(e4, d4) {
                                if (e4) console.error(e4);
                                return res.jsonp({
                                    item: _.assign(data, {
                                        wifipw: d4
                                    })
                                });
                            });

                        });
                    });
                } else return res.status(500).jsonp({
                    error: 'Cannot create venue or duplicates'
                });
            });
        } else return res.jsonp({
            error: 'No foursquare access_token'
        });
    });
};

exports.getOne = function(req, res) {
    if (!req.venue) return res.jsonp({
        error: 'Not found'
    });
    WifiPwModel.find({
        _venueId: req.venue._id
    }).lean().exec(function(e1, d1) {
        if (e1) console.error(e1);
        res.jsonp({
            venue: req.venue,
            wifipw: d1
        });
    });
};

exports.load = function(req, res, next, id) {
    VenueModel.load(id, function(err, doc) {
        if (err || !doc) return res.status(404).jsonp({
            error: 'Not found'
        });
        req.venue = doc;
        next();
    });
};