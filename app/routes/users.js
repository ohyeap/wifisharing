'use strict';

var users = require('../controllers/users');

module.exports = function(app, passport) {

	/**
	 * Get Foursquare auth -> redirect to callback link
	 * @trihuynh
	 */
	app.get('/auth/foursquare', users.authFoursquare);

	/**
	 * Get accesstoken from callback code
	 * @trihuynh
	 */
	app.get('/auth/foursquare/callback', users.callbackFoursquare);

	/**
	 * Login via Facebook
	 */
	app.post('/auth/facebook', users.authFacebook);
	// app.get('/auth/facebook/callback',users.authFacebook);
	
	app.get('/auth/fakeToken', users.fakeToken);

	// app.get('/signout', users.signout);
	app.get('/api/users/me', users.me);
	app.get('/auth/renewtoken', users.renewtoken);

	// Setting up the userId param
	// app.param('userId', users.user);

};