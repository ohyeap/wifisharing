'use strict';

// Venue routes use venue controller
var venues = require('../controllers/venues'),
	wifipws = require('../controllers/wifipws');

module.exports = function(app) {

	app.get('/venues/explore', venues.explore);
	app.post('/api/venue', venues.create);
	// app.get('/api/venue/:venueId', venues.getOne);
	app.get('/venue/:venueId/wifipw', wifipws.getAll);
	app.post('/api/venue/:venueId/wifipw', wifipws.create);
	app.param('venueId', venues.load);

};