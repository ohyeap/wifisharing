'use strict';

/**
 * Module dependencies.
 */
var index = require('../controllers/index');

module.exports = function(app) {

	// Home route
	var index = require('../controllers/index');
	app.get('/', index.render);

};