'use strict';

// WifiPw routes use wifipws controller
var wifipws = require('../controllers/wifipws');

/**
 * WifiPw authorization helpers
 * @trihuynh
 */
// var hasAuthorization = function(req, res, next) {
//     if (req.WifiPw.user.id !== req.user.id) {
//         return res.send(401, 'User is not authorized');
//     }
//     next();
// };

module.exports = function(app) {    
    app.put('/api/wifipw/:wifipwId/like', wifipws.like);
    app.get('/wifipw/:wifipwId', wifipws.getOne);
    // app.put('/api/wifipw/:wifipwId', hasAuthorization, wifipws.update);
    // app.del('/api/wifipw/:wifipwId', hasAuthorization, wifipws.destroy);

    // Finish with setting up the wifipwId param
    app.param('wifipwId', wifipws.load);
};