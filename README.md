WIFI PASSWORD SHARING
=========
# Outsource
## Dev by Ohyeap Team


Add to /node_modules/node-foursquare/lib/venues.js
--------------

```js
/**
   * Add Venue
   * @trihuynh
   */
  function add(params, accessToken, callback) {
    logger.enter('addVenue');

    if (!params) {
      logger.error('add: Params is required.');
      callback(new Error('Venue.add: Params is required.'));
      return;
    }

    if (!params.name) {
      logger.error('add: Venue name is required.');
      callback(new Error('Venue.add: Venue name is required.'));
      return;
    }

    if (!params.ll) {
      logger.error('add: Venue lat/lng is required.');
      callback(new Error('Venue.add: Venue lat/lng is required.'));
      return;
    }

    params = params || {};

    core.callApi('/venues/add', accessToken, params, callback, 'POST');
  }
  // ----
  ...
  return {
    'add':add,
  ...
```
